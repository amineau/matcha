"use strict";

const jwt = require("jsonwebtoken");
const UserQuery = require("../models/shema/user");

const Query = new UserQuery();

module.exports = (req, res, next) => {
  const token = req.headers["matcha-token"];
  jwt.verify(token, process.env.API_TOKEN, (err, decoded) => {
    if (err) {
      return res.json({
        success: false,
        err,
      });
    }
    Query.Connection({ id: decoded.id });
    const life = parseInt(decoded.exp - Date.now() / 1000);
    //console.log('Token expires in ' + life + ' sec')
    req.decoded = decoded;
    next();
  });
};
